## coding:utf-8
from bottle import Bottle, run, template, redirect, request, static_file
from bottle import jinja2_template as template
from bottle import jinja2_view as view
import sqlite3
import datetime
app = Bottle()

# -------------------------------------------
#定義
# -------------------------------------------
open_time = 7
close_time = 19
reservation_code = 0


#現在日時の取得
now = datetime.datetime.today()
dt = now.date().strftime('%Y/%m/%d')

#データベース接続
dbname = 'tsumagari.db'
conn = sqlite3.connect(dbname)
#sqlite操作のカーソルオブジェクト作成
c = conn.cursor()

try:
    
    #同じ名前のテーブルを削除
    c.execute("DROP TABLE IF EXISTS reservations")
    c.execute("DROP TABLE IF EXISTS seats")
    #テーブル作成
    c.execute(
        '''
            CREATE TABLE IF NOT EXISTS reservations(
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              seat_id INTEGER,
              customer_name TEXT,
              time INTEGER,
              stay_times INTEGER,
              reservation_code INTEGER,
              promotion_discount_rate INTEGER,
              is_arrived INTEGER,
              is_thinning INTEGER,
              is_event INTEGER,
              CONSTRAINT reservations_FK_seat_id FOREIGN KEY (seat_id) REFERENCES seats(id)
            );
        '''
    )
    #c.execute("CREATE TABLE IF NOT EXISTS reservations (id INTEGER PRIMARY KEY AUTOINCREMENT, seat_id INTEGER, customer_name TEXT)")
    c.execute("CREATE TABLE IF NOT EXISTS seats (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, number_of_seats INTEGER, Reserved INTEGER)")
    #座席テーブルデータ作成
    seats = [(1,"A席",4,0),
             (2,"B席",4,0),
             (3,"C席",4,0),
             (4,"D席",4,0),
             (5,"E席",2,0),
             (6,"F席",2,0),]
    
    c.executemany('insert into seats(id,name,number_of_seats,Reserved) values(?,?,?,?)',seats)
    

    #ためしに一つ予約情報を追加 ※プログラム変更に伴い無効
    #c.execute("INSERT INTO reservations(seat_id,customer_name,time,stay_times,reservation_code,is_arrived) VALUES (1,'予約下太郎',10,2,25,0)")
except sqlite3.Error as e:
    print('sqlite3.Error occurred:', e.args[0])

conn.commit()
conn.close()


# -------------------------------------------
# ルーティング
# -------------------------------------------
@app.route('/')
@view('index')
def index():
    return {}


# @app.route('/reserve')
# @view('reserve')
# def index():
#     return {}

@app.route("/confirm")
@view('confirm')
def confirm():
    return {}

@app.route('/confirm')
@view('confirm')
def dbtest():
    seats_list = get_seats()
    time_list = get_time_list()
    vacant_list = []
    
    '''for seat in seats_list:
        reserve_flag.append([])
        reserve_time = get_reserve_list(seat['id'])
        res_time = [int(d.get("time")) for d in reserve_time]
        if len(reserve_time) > 0:
            reserve_flag.append((seat['id'],0,res_time))
        else:
            reserve_flag.append((seat['id'],1,res_time))'''
    
    #山本さんのコードを借りて動かしている
    #他の処理や機能を作成したら書き直す
    for time in time_list:
        vacant_list.append([])
        for seat in seats_list:
            reserve = get_reserve_list(seat['id'], time)
            if len(reserve) > 0:
                vacant_list[time - open_time].append((seat['id'], 0))
            else:
                vacant_list[time - open_time].append((seat['id'], 1))

    
    return template('confirm', seats_list=seats_list, time_list=time_list, vacant_list = vacant_list, open_time = open_time)


@app.route("/reserve/<seat_id:int>")
@view('reserve')
def reserve(seat_id):
    seats_list = get_seats()
    seat = get_seat(seat_id)
    time_list = get_time_list()
    return template('reserve',time_list=time_list,seats_list=seats_list,seat=seat)

@app.route("/thanks")
@view('thanks')
def thanks():
    return {}


@app.route("/dbtest")
def reserve():
    return template('dbtest')


@app.route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./static')

# methodにPOSTを指定して、add関数を実装する
@app.route("/add", method="POST")
def add():
    global reservation_code
    reservation_code += 1
    seat_id = request.forms.get("seat_id")
    customer_name = request.forms.getunicode("customer_name")
    time = request.forms.getunicode("Visit_time")
    stay_times = request.forms.getunicode("Utilization_time")

    reserve = [(seat_id,
                customer_name,
                time,
                stay_times,
                reservation_code),] 
    save_todo(reserve)
    #1コマだけ空席状況画面に反映させる処理を考える
    update_seats(seat_id)
    return redirect("/thanks")

#営業時間取得
def get_time_list():
    return range(open_time,close_time)


# -------------------------------------------
#データベース処理
# -------------------------------------------
#座席テーブル情報取得
def get_seats():
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    gseats = "select id, name,number_of_seats,Reserved from seats"
    c.execute(gseats)
    seats_list = []
    for row in c.fetchall():
        seats_list.append({
            "id": row[0],
            "name": row[1],
            "number_of_seats":row[2],
            "Reserved":row[3]
        })
    
    
    return seats_list

def get_seat(seat_id):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    select = "select id, name, number_of_seats from seats where id=?"
    c.execute(select,(seat_id,))
    seat = []
    row = c.fetchone()
    seat = ({
            "id": row[0],
            "name": row[1],
            "number_of_seats":row[2],
    })
    conn.close()
    return seat

#座席ID又は予約済みフラグ取得処理を作る

#予約テーブル情報取得
def get_reservations():
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    select = "select id, seat_id, customer_name, time, stay_times, reservation_code, promotion_discount_rate, is_arrived, is_thinning, is_event from reservations"
    c.execute(select)
    reservations = []
    for row in c.fetchall():
        reservations.append({
            "id": row[0],
            "seat_id": row[1],
            "customer_name":row[2],
            "time":row[3],
            "stay_times":row[4],
            "reservation_code":row[5],
            "promotion_discount_rate":row[6],
            "is_arrived":row[7],
            "is_thinning":row[8],
            "is_event":row[9]
        })
    conn.close()
    return reservations

def get_reserve_list(seat_id,time):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    select = (
        '''
        SELECT reservations.time, reservations.stay_times
        from seats, reservations
        WHERE seats.id = reservations.seat_id
        and seats.id = :seat_id
        and reservations.time <= :time
        and reservations.time + reservations.stay_times > :time
        '''
    )
    c.execute(select,{'seat_id':seat_id,'time':time,})
    reserve_list = []
    for row in c.fetchall():
        reserve_list.append({
            "id": row[0],
            "is_arrived": row[1],
        })

    conn.close()
    return reserve_list

def update_seats(seat_id):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    update = "UPDATE seats SET Reserved=1 WHERE id=?"
    c.execute(update,(seat_id,))
    conn.commit()
    conn.close()

#予約入力情報追加
def save_todo(reserve):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    c.executemany('insert into reservations(seat_id,customer_name,time,stay_times,reservation_code,is_arrived, is_thinning, is_event) values(?,?,?,?,?,0,0,0)',reserve)
    conn.commit()
    conn.close()
   

#予約情報削除
def delete_todo(id):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    delete = "delete from reservations where id=?"
    c.execute(delete, (id,))
    conn.commit()
    conn.close()


# テスト用のサーバをlocalhost:8080で起動する
# reloader=Trueにより、ソースを書き換えると自動的に再起動される

if __name__ == "__main__":
    run(app, host='localhost', port=8080, debug=True, reloader=True)