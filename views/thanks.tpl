{% extends 'base.tpl' %}

{% block content %}
<div class="container">
    <div class="section">
        <h1 class="is-size-3">予約完了</h1>
        <div class="mt-6">
            <p>ご予約を受けたわまりました。</p>
            <p>予約内容の確認はTOPページの予約確認状況よりご確認ください。</p>
        </div>
        <a class="is-flex button is-info mt-6" href="/">TOPへ戻る</a>
    </div>
</div>
{% endblock %}