{% extends 'base.tpl' %}

{% block content %}
<div class="container">
    <div class="section">
        <h1 class="is-size-3">予約詳細受付</h1>
        <div class="mt-6">
            <img src="/static/img/zaseki.png" alt="座席">
            <p class="mt-3">予約をご希望の方は、以下のフォームにご記入ください</p>
            <div class="mt-6">
                <form class="is-flex is-flex-direction-column" name="reserve" method="POST" action="/add">
                    <label class="is-flex is-flex-direction-column">座席指定:
                      <select name="seat_id">
                        <option value="{{seat["id"]}}">{{seat["name"]}}</option>
                        <!--座席リストでfor文で繰り返して作成　条件で入力情報を元に初期選択を決める-->
                        {%for seats in seats_list %}
                        <option>{{seats["name"]}}</option>
                        {% endfor %}
                      </select>
                    </label>
                    <label class="is-flex is-flex-direction-column mt-3">代表者名:<input type="text" name="customer_name" required /></label>
                    <label class="is-flex is-flex-direction-column mt-3">ご予約時刻:
                      <select class="" name="Visit_time" required>
                        <option>選択</option>
                        {%for time in time_list %}
                        <option value="{{time}}">{{time}}:00</option>  
                        {% endfor %}
                      </select>
                    </label>
                    <label class="is-flex is-flex-direction-column mt-3">ご利用時間:
                      <select class="" name="Utilization_time" required>
                        <option>選択</option>
                        <option value="1">1時間</option>
                        <option value="2">2時間</option>
                      </select>
                    </label>
                    <input class="mt-6 button is-primary" type="submit" value="予約する"></input>
                    <button class="reserve button is-info mt-3" type="button" onclick="location.href='/confirm'">戻る</button>
                  </form>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="section">
        <form class="is-flex is-flex-direction-column">
            <!-- <button class="reserve button is-warning" type="button" onclick="location.href='dbtest'">テスト用:予約情報テーブル確認</button> -->
            
          </form>
    </div>
</div>

{% endblock %}