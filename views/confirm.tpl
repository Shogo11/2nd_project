{% extends 'base.tpl' %}

{% block content %}
<div class="container">
    <div class="section">
        <h1 class="is-size-3">予約受付</h1>
        <div class="mt-6">
            <h2 class="is-size-4">座席表</h2>
            <img src="/static/img/zaseki.png" alt="座席">
            <div class="mt-6">
                <p>予約日を選択後、予約時間とテーブルを選択してください。</p>
                <form name="calendar">
                    <input type="date" class="mt-3">
                  </form>
                <table class="biz-hour mt-3">
                    <thead class="">
                     <tr>
                       <th class="py-3 pl-2">営業時間</th>
                       {% for seat in seats_list %}
                       <th class="seat_name has-text-centered py-3" value="{{seat['id']}}">{{seat['name']}}</th>
                       {% endfor %}
                     </tr>
                    </thead>
                    <tbody>
                        {% for time in time_list %}
                        <tr>
                            <td class="Reservation_time py-3 pl-3">{{time}}:00</td>
                            {% for vacant in vacant_list[time - open_time] %}
                                {% if vacant[1] == 1 %}
                                    <td class="has-text-centered py-3"><!--空席ならリンク --><a href="/reserve/{{vacant[0]}}" class="has-text-danger-dark">○</a></td>
                                {% else %}
                                    <td class="has-text-centered py-3 has-text-grey">✕</td>
                                {% endif %}
                            {% endfor %}
                        </tr>
                    {% endfor %}
                    </tbody>
                   </table>
            </div>
        </div>
        <a class="is-flex button is-info mt-6" href="/">TOPに戻る</a>
    </div>
</div>
{% endblock %}