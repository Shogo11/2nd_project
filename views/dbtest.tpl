{% extends "base.tpl" %}

{% block content %}

<div class="container">
    <div class="section">
        <h1 class="is-size-3">予約情報テーブル</h1>
        <div class="mt-6">
            <h2 class="is-size-4">座席表</h2>
            <img src="/static/img/zaseki.png" alt="座席">
            <div class="mt-6">
                <table class="">
                    <tr>
                      <th>ID</th>
                      <th>座席ID</th>
                      <th>予約者名</th>
                      <th>予約時間</th>
                      <th>滞在時間(h)</th>
                      <th>予約コード</th>
                      <th>割引</th>
                      <th>来店フラグ</th>
                      <th>予約調整フラグ</th>
                      <th>イベント登録</th>
                    </tr>
                    {%for res in reservations %}
                    <tr>
                        <td>{{res["id"]}}</td>
                        <td>{{res["seat_id"]}}</td>
                        <td>{{res["customer_name"]}}</td>
                        <td>{{res["time"]}}:00</td>
                        <td>{{res["stay_times"]}}</td>
                        <td>{{res["reservation_code"]}}</td>
                        <td>{{res["promotion_discount_rate"]}}</td>
                        <td>{{res["is_arrived"]}}</td>
                        <td>{{res["is_thinning"]}}</td>
                        <td>{{res["is_event"]}}</td>
                    </tr>
                    {% endfor %}
                  </table>
                
                  <h1>座席情報テーブル</h1>
                  <table border="1">
                    <tr>
                      <th>座席番号</th>
                      <th>座席名</th>
                      <th>座席人数</th>
                      <th>予約状況</th>
                    </tr>
                    {% for seat in seat_list %}
                    <tr>
                        <td>{{seat["id"]}}</td>
                        <td>{{seat["name"]}}</td>
                        <td>{{seat["number_of_seats"]}}</td>
                        <td>
                            {% if seat["Reserved"] == 0 %}
                            <!-- 予約されていない場合の処理 -->
                            {% else %}
                            予約済み
                            {% endif %}
                        </td>
                    </tr>
                    {% endfor %}
                  </table>
                  <button type="button" onclick="history.back()">戻る</button>
            </div>
        </div>
        <a class="is-flex button is-info mt-6" href="/">TOPに戻る</a>
    </div>
</div>
    
{% endblock %}