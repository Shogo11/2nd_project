<!DOCTYPE html>
<html lang="jp">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
	<link rel="stylesheet" href="/static/css/base.css">
	<title>{% block title %}{% endblock %}</title>
	<script src="/static/js/base.js"></script>
</head>
	<body>
		<header class="container">
			<nav class="navbar" role="navigation" aria-label="main navigation">
				<div class="navbar-brand">
				  <a class="navbar-item" href="/">
					<img src="/static/img/logo.png" width="112" height="28">
				  </a>
				</div>
			</nav>
		</header>
			<main>
				{% block content %}{% endblock %}
			</main>
		<footer>
		</footer>
	</body>
</html>