{% extends 'base.tpl' %}

{% block content %}
<div class="hero hero_top_image is-large">
    <div class="hero-body">
        <div class="container"></div>
    </div>
</div>

<div class="container">
    <div class="section is-flex is-flex-direction-column is-justify-content-center">
        <a href="/confirm" class="button mb-4 is-primary">席の新規予約</a>
        <!-- <a class="button mb-4">Anchor</a> -->
        <a class="button is-link mb-4">ご予約確認</a>
        <a class="button is-warning" href="/dbtest">テスト用:予約情報テーブル確認</a>
    </div>
</div>

{% endblock %}